package br.uefs.model;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import br.uefs.util.Calc;
import br.uefs.util.FileHandler;
import br.uefs.util.FileHandler.RankingMethod;
import br.uefs.util.ModelOrganism;
import br.uefs.util.Preprocessor;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Evaluation;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.lazy.IBk;
import weka.core.Instances;

/**
 * 
 * @author luan
 *
 */
public class Classifier {

	private int seed;
	private int folds;

	private ModelOrganism organism;
	private Preprocessor preprocessor;

	private HashMap<RankingMethod, Instances> resultDatasets;
	
	private HashMap<RankingMethod, Double> gMeans;
	private double stdGMean;
	
	private HashMap<RankingMethod, List<Instances>> trainingDatasets;
	private HashMap<RankingMethod, List<Instances>> testingDatasets;

	public Classifier(ModelOrganism organism, HashMap<ModelOrganism, List<List<GOTerm>>> partial,
			HashMap<ModelOrganism, List<GOTerm>> mean, HashMap<ModelOrganism, List<GOTerm>> median,
			HashMap<ModelOrganism, List<GOTerm>> weighted) throws Exception {

		setSeed(1);
		setFolds(10);

		this.setPreprocessor(new Preprocessor(organism));
		this.setgMeans(new HashMap<>());
		//setResultDatasets(preprocessor.loadAllDatasets(partial.get(organism), mean.get(organism), median.get(organism), weighted.get(organism))); // class index is
		// set in the preprocessor
		setOrganism(organism);

		loadTrainingData();
		loadTestingData();
	}
	
	public Classifier(ModelOrganism organism) throws Exception {
		
		setSeed(1);
		setFolds(10);

		this.setPreprocessor(new Preprocessor(organism));
		setOrganism(organism);
	}
	
	public double[] classifyNaiveBayes(Instances training, Instances testing) throws Exception {
		
		NaiveBayes naive = new NaiveBayes();
		
		return this.crossValidation(training, testing, naive);
	}
	
	public double[] classifyKNN(Instances training, Instances testing) throws Exception {
		
		// instantiates an ibk (or knn) object setting k = 1 on constructor's parameter
		IBk knn = new IBk(1);
		
		// JaccardDistance is a created class (doesn't belong to WEKA API) that implements DistanceFunction, OptionHandler, Serializable, RevisionHandler interfaces (belongs to WEKA API)
		JaccardDistance jaccard = new JaccardDistance();
		
		// defines the jaccard distance to be the distance function on knn's search algorithm
		knn.getNearestNeighbourSearchAlgorithm().setDistanceFunction(jaccard);
		
		return this.crossValidation(training, testing, knn);
	}

	// worth seeing:
	// http://stackoverflow.com/questions/10437677/cross-validation-in-weka

	public void classifyKNN() throws Exception {

		IBk knn = new IBk(1); // instantiates an ibk (or knn) object setting k = 1 on constructor's parameter
		JaccardDistance jaccard = new JaccardDistance(); // JaccardDistance is a created class (doesn't belong to WEKA API) that implements DistanceFunction, OptionHandler, Serializable, RevisionHandler interfaces (belongs to WEKA API)

		knn.getNearestNeighbourSearchAlgorithm().setDistanceFunction(jaccard); // defines the jaccard distance to be the distance function on knn's search algorithm

		for (RankingMethod method : resultDatasets.keySet()) {

			//Log log = new Log();
			Instances data = new Instances(resultDatasets.get(method));

			double gMean = this.crossValidationGMean(data, knn);
			this.getgMeans().put(method, gMean);

			//log.finalResult(randomData.numInstances(), correct, incorrect, sensitivity, specificity);
			// log.printStandardDeviation(correct, incorrect);
		}
	}
	
	public void classifyNaiveBayes() throws Exception {
		
		NaiveBayes naive = new NaiveBayes();

		for (RankingMethod method : resultDatasets.keySet()) {

			//Log log = new Log();
			Instances data = new Instances(resultDatasets.get(method));
			double gMean = this.crossValidationGMean(data, naive);
			
			this.getgMeans().put(method, gMean);

			//log.finalResult(randomData.numInstances(), correct, incorrect, sensitivity, specificity);
			// log.printStandardDeviation(correct, incorrect);
		}

	}
	
	public double[] crossValidation(Instances training, Instances test, AbstractClassifier classifierAlgorithm) throws Exception {
		
		double truePos = 0, trueNeg = 0, falsePos = 0, falseNeg = 0;
		
		Evaluation eval = new Evaluation(training);
		classifierAlgorithm.buildClassifier(training);

		eval.evaluateModel(classifierAlgorithm, test);
		
		truePos = eval.numTruePositives(1);
		trueNeg = eval.numTrueNegatives(1);
		falsePos = eval.numFalsePositives(1);
		falseNeg = eval.numFalseNegatives(1);
		
		// [sensitivity, specificity]
		return new double[]{Calc.normalise(truePos / (truePos + falseNeg), 1), Calc.normalise(trueNeg / (trueNeg + falsePos), 1)};
	}
	
	public double crossValidationGMean(Instances randomData, AbstractClassifier classifierAlgorithm) throws Exception {
		
		Random random = new Random(seed);
		double[] correct = new double[folds], incorrect = new double[folds], sensitivity = new double[folds],
				specificity = new double[folds];
		double totalSen = 0, totalSpe = 0;

		randomData.randomize(random);
		randomData.stratify(folds);

		// List<Instances> trainingData = trainingDatasets.get(method);
		// List<Instances> testingData = testingDatasets.get(method);
		// System.out.println("=======" + method.name() + "=======");
		for (int n = 0; n < folds; n++) {

			double truePos = 0, trueNeg = 0, falsePos = 0, falseNeg = 0;

			Instances train = randomData.trainCV(folds, n, random);
			Instances test = randomData.testCV(folds, n);
			// Instances train = trainingData.get(n);
			// Instances test = testingData.get(n);

			Evaluation eval = new Evaluation(train);
			classifierAlgorithm.buildClassifier(train);

			eval.evaluateModel(classifierAlgorithm, test);

			correct[n] = eval.correct();
			incorrect[n] = eval.incorrect();

			/**
			 * Eval's Confusion Matrix is as follows:
			 * 
			 * TP FN | a = 1 (positive class) FP TN | b = 0 (negative class)
			 * 
			 * The class index on numTruePositives() method and its
			 * relatives refers to the index on an array of classes, i.e.,
			 * considering the classes upward we would have an array of
			 * [1,0]. If I indicate index 0 as the index of the positive
			 * class, then class 1 (or a) would be considered the positive
			 * class.
			 */

			truePos = eval.numTruePositives(1);
			trueNeg = eval.numTrueNegatives(1);
			falsePos = eval.numFalsePositives(1);
			falseNeg = eval.numFalseNegatives(1);

			sensitivity[n] = Calc.normalise(truePos / (truePos + falseNeg), 1);
			specificity[n] = Calc.normalise(trueNeg / (trueNeg + falsePos), 1);
			
			totalSen += sensitivity[n];
			totalSpe += specificity[n];
			
		}
		
		return Calc.gmean(totalSen, totalSpe)*100;
	}
	
	public void classifyNaiveBayesOriginalDataset() throws Exception {
		
		NaiveBayes naive = new NaiveBayes();

		Instances data = new Instances(this.preprocessor.getOriginalDataset());

		double gMean = this.crossValidationGMean(data, naive);
		this.setStdGMean(gMean);

	}


	public void classifyKNNOriginalDataset() throws Exception {


		IBk knn = new IBk(1);
		JaccardDistance jaccard = new JaccardDistance();

		knn.getNearestNeighbourSearchAlgorithm().setDistanceFunction(jaccard);
		//Log log = new Log();
		Instances data = new Instances(this.preprocessor.getOriginalDataset());

		double gMean = this.crossValidationGMean(data, knn);
		this.setStdGMean(gMean);

		//log.finalResult(randomData.numInstances(), correct, incorrect, sensitivity, specificity);
		// log.printStandardDeviation(correct, incorrect);
	}

	@Deprecated
	private void loadTrainingData() {

		HashMap<RankingMethod, List<Instances>> training = new HashMap<>();

		for (RankingMethod method : resultDatasets.keySet()) {

			List<Instances> data = null;
			String filename = "results/cross-validation/" + method.name() + "/n-train";

			try {

				data = FileHandler.readFoldFiles(filename, organism, folds); // class
																				// index
																				// is
																				// set
																				// when
																				// fold
																				// is
																				// read

			} catch (Exception e) {

				try {

					data = this.foldResultDatasets(method); // class index is
															// set in the method

				} catch (IOException e1) {

					e1.printStackTrace();
				}
			}

			training.put(method, data);
		}

		setTrainingDatasets(training);
	}
	
	@Deprecated
	private void loadTestingData() {

		HashMap<RankingMethod, List<Instances>> testing = new HashMap<>();

		for (RankingMethod method : RankingMethod.values()) {

			List<Instances> data = null;
			String filename = "results/cross-validation/" + method.name() + "/n-test";

			try {

				data = FileHandler.readFoldFiles(filename, organism, folds); // class
																				// index
																				// is
																				// set
																				// when
																				// fold
																				// is
																				// read

			} catch (Exception e) {

				try {

					data = this.foldResultDatasets(method); // class index is
															// set in the method

				} catch (IOException e1) {

					e1.printStackTrace();
				}
			}

			testing.put(method, data);
		}

		setTestingDatasets(testing);
	}
	
	@Deprecated
	private List<Instances> foldResultDatasets(RankingMethod method) throws IOException {

		List<Instances> training = new ArrayList<>();

		Instances randomData = new Instances(this.resultDatasets.get(method));
		Random random = new Random(this.seed);

		randomData.randomize(random);

		for (int n = 0; n < this.folds; n++) {

			Instances train = randomData.trainCV(folds, n, random);
			Instances test = randomData.testCV(folds, n);

			FileHandler.saveDatasetFile("cross-validation/" + method.name() + "/" + n + "-train", organism, train);
			FileHandler.saveDatasetFile("cross-validation/" + method.name() + "/" + n + "-test", organism, test);

			if (train.classIndex() == -1)
				train.setClassIndex(train.numAttributes() - 1);

			training.add(train);
		}

		return training;
	}

	public int getSeed() {
		return seed;
	}

	public void setSeed(int seed) {
		this.seed = seed;
	}

	public int getFolds() {
		return folds;
	}

	public void setFolds(int folds) {
		this.folds = folds;
	}

	public ModelOrganism getOrganism() {
		return organism;
	}

	public void setOrganism(ModelOrganism organism) {
		this.organism = organism;
	}

	public HashMap<RankingMethod, Instances> getResultDatasets() {
		return resultDatasets;
	}

	public void setResultDatasets(HashMap<RankingMethod, Instances> resultDatasets) {
		this.resultDatasets = resultDatasets;
	}

	public HashMap<RankingMethod, List<Instances>> getTrainingDatasets() {
		return trainingDatasets;
	}

	public void setTrainingDatasets(HashMap<RankingMethod, List<Instances>> trainingDatasets) {
		this.trainingDatasets = trainingDatasets;
	}

	public HashMap<RankingMethod, List<Instances>> getTestingDatasets() {
		return testingDatasets;
	}

	public void setTestingDatasets(HashMap<RankingMethod, List<Instances>> testingDatasets) {
		this.testingDatasets = testingDatasets;
	}

	public Preprocessor getPreprocessor() {
		return preprocessor;
	}

	public void setPreprocessor(Preprocessor preprocessor) {
		this.preprocessor = preprocessor;
	}

	public HashMap<RankingMethod, Double> getgMeans() {
		return gMeans;
	}

	public void setgMeans(HashMap<RankingMethod, Double> gMeans) {
		this.gMeans = gMeans;
	}

	public double getStdGMean() {
		return stdGMean;
	}

	public void setStdGMean(double stdGMean) {
		this.stdGMean = stdGMean;
	}
}
