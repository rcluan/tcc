package br.uefs.model;

import java.util.ArrayList;
import java.util.List;

public class GOTerm implements Comparable<GOTerm>{

	private String term;

	private double rankValue;
	private int termPos;
	private double averagePos;

	private String dataset;
	private String nameSpace;
	private String name;
	private String definition;
	
	private List<String> synonym;
	private List<GOTerm> ancestors;
	private List<GOTerm> descendants;
	private List<Integer> positions;

	public String getTerm() {
		return term;
	}

	public void setTerm(String term) {
		this.term = term;
	}

	public double getRankValue() {
		return rankValue;
	}

	public void setRankValue(double rankValue) {
		this.rankValue = rankValue;
	}

	public int getTermPos() {
		return termPos;
	}

	public void setTermPos(int rankPos) {
		this.termPos = rankPos;
	}
	
	public double getAveragePos() {
		return averagePos;
	}

	public void setAveragePos(double averagePos) {
		this.averagePos = averagePos;
	}

	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getNameSpace() {
		return nameSpace;
	}

	public void setNameSpace(String nameSpace) {
		this.nameSpace = nameSpace;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDefinition() {
		return definition;
	}

	public void setDefinition(String definition) {
		this.definition = definition;
	}

	public List<String> getSynonyms() {
		return synonym;
	}

	public void setSynonyms(List<String> synonym) {
		this.synonym = synonym;
	}

	public List<GOTerm> getAncestors() {
		return ancestors;
	}

	public void setAncestors(List<GOTerm> ancestors) {
		this.ancestors = ancestors;
	}

	public List<GOTerm> getDescendants() {
		return descendants;
	}

	public void setDescendants(List<GOTerm> descendants) {
		this.descendants = descendants;
	}

	public List<Integer> getPositions() {
		return positions;
	}

	public void setPositions(List<Integer> positions) {
		this.positions = positions;
	}

	public GOTerm(String term, double rankValue, int termPos) {

		this.setTerm(term);
		this.setRankValue(rankValue);
		this.setTermPos(termPos);

		this.setAncestors(new ArrayList<>());
		this.setDescendants(new ArrayList<>());
		this.setPositions(new ArrayList<>());
	}

	public GOTerm(String term, String dataset) {

		this.setTerm(term);
		this.setDataset(dataset);

		this.setRankValue(0);

		this.setAncestors(new ArrayList<>());
		this.setDescendants(new ArrayList<>());
		this.setPositions(new ArrayList<>());
	}
	
	/**
	 * Method to analyse whether to select a term according to its ranking value
	 * and ancestors. A lower position indicates a better ranked term
	 * 
	 * @return whether the term should be selected or not
	 */
	public boolean isBetterQualified(){
		
		for (GOTerm descendant : this.getDescendants()) {
			
			if(this.getTermPos() >= descendant.getTermPos())
				return false;

		}

		return true;
	}
	
	/**
	 * Method to analyse whether to select a term according to its average position value
	 * and ancestors. A lower average indicates a better ranked term
	 * 
	 * @return whether the term should be selected or not
	 */
	public boolean isBetterQualifiedAvg(){
		
		for (GOTerm descendant : this.getDescendants()) {
			
			if(this.getAveragePos() >= descendant.getAveragePos())
				return false;

		}

		return true;
	}
	
	

	@Override
	public int compareTo(GOTerm term) {
		
		return (new Double(term.getRankValue()).compareTo(this.getRankValue()));
	}
	
	@Override
	public boolean equals(Object term){	
		
		if(((GOTerm) term).getRankValue() == this.getRankValue())
			return true;
		
		return false;
	}
	
	@Override
	public String toString() {
		
		return this.getRankValue() + " " + this.getTermPos() + " " + this.getTerm();
	}
}
