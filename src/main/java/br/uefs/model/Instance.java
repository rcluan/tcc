package br.uefs.model;

import java.util.ArrayList;
import java.util.List;

public class Instance {
	
	private List<String> GOTerms;
	private int classValue;
	
	public Instance(){
		
		setGOTerms(new ArrayList<>());
	}

	public List<String> getGOTerms() {
		return GOTerms;
	}

	public void setGOTerms(List<String> gOTerms) {
		GOTerms = gOTerms;
	}

	public int getClassValue() {
		return classValue;
	}

	public void setClassValue(int classValue) {
		this.classValue = classValue;
	}
}
