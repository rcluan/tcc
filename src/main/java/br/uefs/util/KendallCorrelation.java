package br.uefs.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.math3.stat.correlation.KendallsCorrelation;

import br.uefs.model.GOTerm;

public class KendallCorrelation {
	
	// http://www.originlab.com/doc/Origin-Help/CorrCoef-Algorithm
		// http://www.statisticssolutions.com/correlation-pearson-kendall-spearman/
		/**
		 * Kendall's and Spearman's correlation are statistically the same. The
		 * different is that, whilst for Spearman's the order that the values are
		 * disposed matters, for Kendall's the important is the number of pairs in
		 * the same and different order.
		 * 
		 * Uses the KendallsCorrelation class from apache's commons math package
		 * 
		 * @return a list of list containing the correlation weights
		 */
	public static void run(Collection<List<GOTerm>> filteredTerms, List<List<Double>> correlationWeights) {
		
		KendallsCorrelation correlation = new KendallsCorrelation();
		
		for (List<GOTerm> X : filteredTerms) {

			List<Double> correlationWeight = new ArrayList<>();
			
			double[] x = KendallCorrelation.listToDoubleArray(X);
			
			for(List<GOTerm> Y : filteredTerms) {
				
				double[] y = KendallCorrelation.listToDoubleArray(Y);
				
				double correlationValue = correlation.correlation(x, y);
				correlationWeight.add(correlationValue);
			}
			
			correlationWeights.add(correlationWeight);
		}
	}

	private static double[] listToDoubleArray(List<GOTerm> list) {
		
		double[] array = new double[list.size()];
		
		for(GOTerm term : list) {
			
			int index = list.indexOf(term);
			array[index] = term.getTermPos();
		}
		
		return array;
	}
}
