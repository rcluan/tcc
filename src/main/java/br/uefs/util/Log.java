package br.uefs.util;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import weka.classifiers.Evaluation;

public class Log {

	private List<String> logs;
	
	public Log(){
		
		logs = new ArrayList<>();
	}
	
	public void results(Evaluation eval, int classIndex) throws Exception{
		
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.CEILING);
		
		logs.removeAll(logs);
		buildLog("Correctly Classified Instances\t\t", Double.toString(eval.correct()), df.format(eval.pctCorrect())+"%");
		buildLog("Incorrectly Classified Instances\t", Double.toString(eval.incorrect()), df.format(eval.pctIncorrect())+"%");
		
		buildLog("True positives\t\t\t\t", Double.toString(eval.numTruePositives(0)));
		buildLog("True negatives\t\t\t\t", Double.toString(eval.numTrueNegatives(0)));
		buildLog("False positives\t\t\t\t", Double.toString(eval.numFalsePositives(0)));
		buildLog("False negatives\t\t\t\t", Double.toString(eval.numFalseNegatives(0)));
		
		buildLog(eval.toMatrixString("=== Confusion matrix ===\n"));
		
		System.out.println("\nFold results\n======\n");
		printLog();
		System.out.println("\n\n======\n");
	}
	
	private void buildLog(String msg, String... contents){
		
		StringBuilder log = new StringBuilder(msg);
		
		for(String content : contents){
			
			log.append(content+"\t");
		}
		
		logs.add(log.toString().trim());
	}
	
	private void printLog(){
		
		for(String log : logs){
			
			System.out.println(log);
		}
	}
	

	public void finalResult(int numInstances, double[] correct, double[] incorrect, double[] sensitivity, double[] specificity) {
		
		DecimalFormat df = new DecimalFormat("#.##");
		df.setRoundingMode(RoundingMode.CEILING);
		
		logs.removeAll(logs);
		
		double totalCorrect = 0, totalIncorrect = 0, totalSen = 0, totalSpe = 0;
		
		for(int n = 0; n < correct.length; n++){
			
			totalCorrect += correct[n];
			totalIncorrect += incorrect[n];
			totalSen += sensitivity[n];
			totalSpe += specificity[n];
			
		}
		
		double meanCorrect = Calc.mean(correct),
				meanIncorrect = Calc.mean(incorrect),
				meanSensititivy = Calc.mean(sensitivity),
				meanSpecificity = Calc.mean(specificity);
		
		double stdDeviationCorrect = Calc.stdDeviation(correct, meanCorrect),
				stdDeviationIncorrect = Calc.stdDeviation(incorrect, meanIncorrect),
				stdDeviationSen = Calc.stdDeviation(sensitivity, meanSensititivy),
				stdDeviationSpe = Calc.stdDeviation(specificity, meanSpecificity);
		
		double stdErrorSen = Calc.stdError(stdDeviationSen, sensitivity.length)*100,
				stdErrorSpe = Calc.stdError(stdDeviationSpe, specificity.length)*100;
		
		double GMean = Calc.gmean(totalSen, totalSpe)*100;
		
		buildLog("Total correct instances\t\t\t", Double.toString(totalCorrect), df.format((totalCorrect/numInstances)*100)+"%\t\t" + df.format(stdDeviationCorrect));
		buildLog("Total incorrect instances\t\t", Double.toString(totalIncorrect), df.format((totalIncorrect/numInstances)*100)+"%\t\t" + df.format(stdDeviationIncorrect));
		
		buildLog("Sensitivity\t\t\t\t", df.format(totalSen*100)+"\t\t\t ±" + df.format(stdErrorSen));
		buildLog("Specificity\t\t\t\t", df.format(totalSpe*100)+"\t\t\t ±" + df.format(stdErrorSpe));
		buildLog("GMean\t\t\t\t\t", df.format(GMean));
		System.out.println("\nTotal results\n======\n");
		printLog();
		System.out.println("\n\n======\n");
	}
}
