package br.uefs.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import br.uefs.model.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * 
 * Class to handle reading and writing on files.
 * 
 * It holds some enumerators that addresses ranking methods, file extensions
 * 
 * @author luan
 *
 */
public class FileHandler {
	
	public enum FileExtension {
		
		txt, arff
	}
	
	public enum RankingMethod {
		
		chi2, ig, gr, relieff, oner, su, aar, war, mr
	}
	
	public static Instances readDatasetFile(String filename, ModelOrganism organism) throws Exception{
		
		String file = "datasets/" + organism.name() + "/" + filename + "." + FileExtension.arff.name();
		DataSource source = new DataSource(file);
		
		Instances data = source.getDataSet();
		
		if(data.classIndex() == -1)
			data.setClassIndex(data.numAttributes() - 1);
		
		return data;
	}
	
	public static List<Instances> readFoldFiles(String filename, ModelOrganism organism, int folds) throws Exception{
		
		List<Instances> foldsData = new ArrayList<>();
		
		for(int n = 0; n < folds; n++){
			filename = filename.replace("/n-", "/"+n+"-");
			String file = "datasets/" + organism.name() + "/" + filename+ "." + FileExtension.arff.name();
			DataSource source = new DataSource(file);
			
			Instances fold = source.getDataSet();
			
			if(fold.classIndex() == -1)
				fold.setClassIndex(fold.numAttributes() - 1);
			
			foldsData.add(fold);
		}
		
		return foldsData;
	}
	
	public static HashMap<String, List<String>> readAncestors(ModelOrganism organism) throws IOException {
		
		HashMap<String, List<String>> organismTerms = new HashMap<>();
		String path = "datasets/" + organism.name() + "/ancestors_" + organism.name().toLowerCase() + "." + FileExtension.txt.name();
		
		String line = null;
		BufferedReader reader = new BufferedReader(new FileReader(path));
		
		while((line = reader.readLine()) != null) {
			
			List<String> ancestors = new ArrayList<>();
			
			String[] terms = line.split(" ");
			
			String term = terms[0]; // the term we want
			
			// ancestors are the second term onward
			ancestors.addAll(Arrays.asList(terms).subList(1, terms.length));
			
			organismTerms.put(term, ancestors);
		}
		
		reader.close();
		return organismTerms;
	}
	
	public static void readInfo(ModelOrganism organism) {
		
		//String path = "datasets/" + organism.name() + "/terms_info_" + organism.name().toLowerCase() + "." + FileExtension.txt.name();
	}
	
	public static List<String> readResultFile(ModelOrganism organism, RankingMethod method) throws IOException{
		
		String file = "datasets/" + organism.name() + "/results/" + method.name() + "." + FileExtension.txt.name();
		
		BufferedReader reader = new BufferedReader(new FileReader(file));
		
		List<String> removedTerms = new ArrayList<>();
		
		String line = null;
		
		while((line = reader.readLine()) != null){
			
			String[] content = line.split("\\s");
			String removedTerm = "";
			
			if(method.equals(RankingMethod.aar) || method.equals(RankingMethod.war) ||
					method.equals(RankingMethod.mr)){
				
				removedTerm = content[0];
			}else{
				
				removedTerm = content[1];
			}
			
			removedTerms.add(removedTerm);
		}
		
		reader.close();
		return removedTerms;
	}
	
	public static HashMap<String, List<Integer>> mapTerms(ModelOrganism organism) throws IOException{
		
		HashMap<String, List<Integer>> terms_genes = new HashMap<>();
		
		String file = "dataset_model/" + organism.name().toLowerCase() + "/datamodel.txt";
		
		BufferedReader reader = new BufferedReader(new FileReader(file));
		
		String line = null;
		
		while((line = reader.readLine()) != null){
			
			String[] content = line.split("\t");
			
			String[] GOTerms = content[1].split(" ");
			
			for(String GOTerm : GOTerms){
				
				if(!GOTerm.equals("")){
					if(terms_genes.get(GOTerm) == null){
						
						terms_genes.put(GOTerm, new ArrayList<>());
					}
					
					List<Integer> genes = terms_genes.get(GOTerm);
					
					if(!genes.contains(Integer.parseInt(content[0]))){
						
						genes.add(Integer.parseInt(content[0]));
						
						terms_genes.put(GOTerm, genes);
					}
				}
			}
			
		}
		
		reader.close();
		
		return terms_genes;
	}
	
	public static HashMap<String, Instance> mapGenes(ModelOrganism organism) throws IOException{
		
		HashMap<String, Instance> terms_genes = new HashMap<>();
		
		String file = "dataset_model/" + organism.name().toLowerCase() + "/datamodel.txt";
		
		BufferedReader reader = new BufferedReader(new FileReader(file));
		
		String line = null;
		
		while((line = reader.readLine()) != null){
			
			String[] content = line.split("\t");
			
			if(terms_genes.get(content[0]) == null){
				
				terms_genes.put(content[0], new Instance());
			}
			
			Instance instance = terms_genes.get(content[0]);
			
			instance.setClassValue(Integer.parseInt(content[2]));
			
			String[] GOTerms = content[1].trim().split(" ");
			
			for(String GOTerm : GOTerms){
				
				instance.getGOTerms().add(GOTerm);
			}
			
			terms_genes.put(content[0], instance);
		}
		
		reader.close();
		
		return terms_genes;
	}

	public static void saveDatasetFile(String filename, ModelOrganism organism, Instances data) throws IOException {
		
		String file = "datasets/" + organism.name() + "/results/" + filename + "." + FileExtension.arff.name();
		ArffSaver saver = new ArffSaver();
		
		saver.setInstances(data);
		saver.setFile(new File(file));
		
		saver.writeBatch();
	}
}
