package br.uefs.util;

import java.util.HashMap;

import br.uefs.model.GOTerm;

public class Graph implements Cloneable {

	private HashMap<String, GOTerm> terms;

	public Graph(ModelOrganism organism) {
		
		this.setTerms(GraphHandler.initialiseGraph(organism));

		//GraphHandler.adjustInfo(this.getTerms(), organism);
		GraphHandler.setAncestors(this.getTerms(), organism);
	}
	
	public Graph clone() {
		
		final Graph clone;
		try {
			clone = (Graph) super.clone();
		} catch ( CloneNotSupportedException ex) {
			
			throw new RuntimeException(ex);
		}
		
		return clone;
	}

	public HashMap<String, GOTerm> getTerms() {
		return terms;
	}

	public void setTerms(HashMap<String, GOTerm> terms) {
		this.terms = terms;
	}
}
