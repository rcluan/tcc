package br.uefs.util;

public class Calc {

	public static double normalise(double d, int k) {
		
		return d/Math.pow(10, k);
	}
	
	public static double total(double[] results) {
		
		double total = 0;
		for(int i = 0; i < results.length; i++) {
			
			total += results[i];
		}
		
		return total;
	}
	

	public static double stdDeviation(double[] results, double mean){
		
		double stdDeviation = 0;
		
		// correct and incorrect have the same length
		
		for(int n = 0; n < results.length; n++){
			
			stdDeviation += Math.pow((results[n] - mean), 2);
		}
		
		return Math.sqrt((stdDeviation / mean));
	}
	
	public static double mean(double[] results){
		
		double mean = 0;
		
		for(int n = 0; n < results.length; n++){
			
			mean += results[n];
		}
		
		return mean / results.length;
	}
	
	public static double stdError(double stdDeviation, int n){
		
		
		return Math.round(stdDeviation/Math.sqrt(n) * 10000.0) / 10000.0;
	}


	public static double gmean(double totalSen, double totalSpe) {
		
		return Math.round(Math.sqrt(totalSen*totalSpe) * 10000.0) / 10000.0;
	}
}
