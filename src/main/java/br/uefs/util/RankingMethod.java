package br.uefs.util;

public enum RankingMethod {
	
	chi2("weka.attributeSelection.ChiSquaredAttributeEval"),
	ig("weka.attributeSelection.InfoGainAttributeEval"),
	gr("weka.attributeSelection.GainRatioAttributeEval"),
	relieff("weka.attributeSelection.ReliefFAttributeEval"),
	oner("weka.attributeSelection.OneRAttributeEval"),
	su("weka.attributeSelection.SymmetricalUncertAttributeEval");
	
	public String evaluationPackage;
	
	private RankingMethod(String evaluationPackage) {
		
		this.evaluationPackage = evaluationPackage;
	}
}
