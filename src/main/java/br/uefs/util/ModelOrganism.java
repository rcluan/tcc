package br.uefs.util;


public enum ModelOrganism {
	
	Fly("DM-fly-GO-threshold-3"),
	Mouse("MM-mouse-GO-threshold-3"),
	Worm("CE-worm-GO-threshold-3"),
	Yeast("SC-yeast-GO-threshold-3");
	
	public String originalDataset;
	
	private ModelOrganism(String originalDataset){
		
		this.originalDataset = originalDataset;
	}
}
