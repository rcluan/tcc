package br.uefs.util;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import br.uefs.model.GOTerm;
import weka.core.Attribute;
import weka.core.Instances;

/**
 * 
 * @author Luan
 * 
 *         Class responsible for handling some requests from the Graph class,
 *         such as initialisation.
 * 
 */
public class GraphHandler {

	/**
	 * Method to initialise the Graph
	 * 
	 * It requests to the FileHandler to open and read a file containing only
	 * the GO terms. The file is read and its information are organized by the
	 * FileHandler's readTerms method.
	 * 
	 * The information provided by the FileHandler is, then, used to build the
	 * nodes (vertices) of the Graph, regardless the GO term's ancestors.
	 * 
	 * @param folder
	 *            the folder containing the text file with all GO terms
	 */
	public static HashMap<String, GOTerm> initialiseGraph(ModelOrganism organism) {

		HashMap<String, GOTerm> terms = new HashMap<>();
		
		Instances data;
		try {
			
			data = FileHandler.readDatasetFile(organism.originalDataset, organism);
			
			
			for(int i = 0; i < data.numAttributes()-1; i++){
				
				Attribute attribute = data.attribute(i);
				
				terms.put(attribute.name(), new GOTerm(attribute.name(), organism.name()));
			}

		} catch (Exception e) {
			
			e.printStackTrace();
		}
		return terms;
	}

	/**
	 * Method to define the ancestors of each GO term in the Graph
	 * 
	 * It requests to the FileHandler to open and read a file containing all
	 * ancestors of each term. The file is read and its information are
	 * organized by the FileHandler's readAncestors method.
	 * 
	 * The information provided by the FileHandler is, then, used to search a GO
	 * term in the hash map that compounds the Graph and to insert the term's
	 * ancestors into its list.
	 * 
	 * @param terms
	 *            the Graph itself with the terms to which the ancestors will be
	 *            added
	 * @param folder
	 *            the folder containing the text file with the ancestors
	 */
	public static void setAncestors(final HashMap<String, GOTerm> terms,
			ModelOrganism organism) {
		
		
		try {
			
			HashMap<String, List<String>> organismTerms = FileHandler.readAncestors(organism);
			
			organismTerms.forEach((term, ancestors) -> {
				
				GOTerm goTerm = terms.get(term);
				
				ancestors.forEach((ancestor) -> {
					
					GOTerm ancestorTerm = terms.get(ancestor);
					
					if(ancestorTerm != null){
						
						ancestorTerm.getDescendants().add(goTerm);
						goTerm.getAncestors().add(ancestorTerm);
					}
				});
				
			});
		} catch (IOException e) {
			
			e.printStackTrace();
		}
	}

	public static void adjustInfo(HashMap<String, GOTerm> terms, ModelOrganism organism) {
		
		/*
		String path = FileHandler.buildPathTermsInfo(folder);
		
		if (FileHandler.checkFile(path)) {

			for (GOTerm term : terms.values()) {

				FileHandler.writeGOTermsInfoFile(term.getTerm(), path);
			}
		}
		
		List<List<String>> fileInfo = FileHandler.readTermsInfo(path);
		
		for(List<String> info : fileInfo){
			
			GOTerm term = terms.get(info.get(0));
			
			
			 * The positions in the array are well known, so it is okay to directly access the variable by its index
			 
			
			term.setName(info.get(1));
			term.setNameSpace(info.get(2));
			//term.setDefinition(info.get(3));
			
			//term.setSynonyms(new ArrayList<>());
			
			//for(int i = 4; i < info.size(); i++)
				//term.getSynonyms().add(info.get(i));
		}
		*/
	}
}
