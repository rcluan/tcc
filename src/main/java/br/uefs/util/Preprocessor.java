package br.uefs.util;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.uefs.model.GOTerm;
import br.uefs.util.RankingMethod;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

/**
 * 
 * Class responsible for preprocessing the original dataset (.arff file) of each
 * Model organism and its result .txt files obtained using the Ranking Analyst.
 * 
 * In case the dataset (.arff file) of the .txt file, the preprocessor is
 * responsible for analysing the contents of the .txt file and making the
 * necessary changes on the original dataset in order to build the result
 * dataset.
 * 
 * @author luan
 *
 */
public class Preprocessor {

	private Instances originalDataset;
	private ModelOrganism organism;
	
	/**
	 * Constructs an Preprocessor to read files from a given model organism
	 * and build their datasets when necessary
	 * 
	 * @param organism the organism the files should be preprocessed
	 * @throws Exception
	 */
	public Preprocessor(ModelOrganism organism) throws Exception {

		originalDataset = FileHandler.readDatasetFile(organism.originalDataset, organism);
		this.organism = organism;
	}
	
	/**
	 * 
	 * Builds a new dataset based on the results of the Feature Selection method 
	 * considering the original dataset of the Preprocessor's model organism.
	 * 
	 * Firstly a copy of the original dataset is made on a new Instances object. 
	 * Afterwards, the removed GO terms are read from a text file by the FileHandler 
	 * and saved in a list of Strings to later have their respective attribute deleted
	 * on the copy of the original dataset.
	 * 
	 * The copy of the original dataset, which now constitutes a new dataset containing 
	 * only the terms not removed by the Feature Selection method, is saved on a .arff file.
	 * 
	 * @param method the ranking method whose results will be represented on the dataset
	 * @param analystResult 
	 * @return the recently created dataset data
	 * @throws IOException
	 */
	public Instances buildDataset(RankingMethod method, List<GOTerm> analystResult) throws IOException {

		Instances data = new Instances(originalDataset);
		
		// excludes class
		for(int index = 0; index < data.numAttributes() - 1; index++) {
			
			Attribute attribute = data.attribute(index);
			
			if(!find(attribute, analystResult)) {
				
				data.deleteAttributeAt(index);
			}
		}
		
		//FileHandler.saveDatasetFile(method.name() + "-" + this.organism.originalDataset, this.organism, data);
		
		if(data.classIndex() == -1)
			data.setClassIndex(data.numAttributes() - 1);
		
		return data;
	}
	
	private boolean find(Attribute attribute, List<GOTerm> analystResult) {
		
		for(GOTerm term : analystResult) {
			
			if(attribute.name().equals(term.getTerm())) {
				
				return true;
			}
		}
		return false;
	}

	/**
	 * 
	 * @param weighted 
	 * @param median 
	 * @param mean 
	 * @param partial 
	 * @return
	 */
	public HashMap<RankingMethod, Instances> loadAllDatasets(List<List<GOTerm>> partial, List<GOTerm> mean, List<GOTerm> median, List<GOTerm> weighted) {

		HashMap<RankingMethod, Instances> results = new HashMap<>();

		for (RankingMethod method : RankingMethod.values()) {

			Instances data = null;
			String filename = "results/" + method.name() + "-" + this.organism.originalDataset;

			try {
				
				// class index is set when the file is read
				data = FileHandler.readDatasetFile(filename, organism);

			} catch (Exception e) {

				try {
					
					List<GOTerm> analystResult = null;
					
					switch (method) {
					case chi2:
						analystResult = partial.get(0);
						break;
					case ig:
						analystResult = partial.get(1);
						break;
					case gr:
						analystResult = partial.get(2);
						break;
					case relieff:
						analystResult = partial.get(3);
						break;
					case oner:
						analystResult = partial.get(4);
						break;
					case su:
						analystResult = partial.get(5);
						break;
					//case aar:
					//	analystResult = mean;
					//	break;
					//case war:
					//	analystResult = weighted;
					//	break;
					//case mr:
					//	analystResult = median;
					//	break;
					}
					
					data = this.buildDataset(method, analystResult);
					
				} catch (IOException e1) {
					
					e1.printStackTrace();
				}
			}

			results.put(method, data);
		}
		
		return results;
	}
	
	/**
	 * 
	 * @param arffFile
	 * @return
	 * @throws Exception
	 */
	public Instances getDatasetContent(String arffFile) throws Exception {

		DataSource source = new DataSource("datasets/" + arffFile + ".arff");

		Instances data = source.getDataSet();

		if (data.classIndex() == -1)
			data.setClassIndex(data.numAttributes() - 1);

		return data;
	}
	
	/**
	 * 
	 * @param textFile
	 * @return
	 * @throws IOException
	 */
	public List<Integer> getSelectedTermsIndexes(String textFile) throws IOException {

		ArrayList<Integer> indexes = new ArrayList<>();

		BufferedReader reader = new BufferedReader(new FileReader("datasets/results/" + textFile + ".txt"));

		String line = null;

		while ((line = reader.readLine()) != null) {

			String[] content = line.split(" ");

			Integer index = Integer.parseInt(content[0]);

			indexes.add(index);
		}

		reader.close();

		return indexes;
	}
	
	public Instances getOriginalDataset(){
		
		return originalDataset;
	}

	public Instances buildFromDataset(Instances dataset, List<GOTerm> analystResult) {
		
		Instances data = new Instances(dataset);
		
		// excludes class
		for(int index = 0; index < data.numAttributes() - 1; index++) {
			
			Attribute attribute = data.attribute(index);
			
			if(!find(attribute, analystResult)) {
				
				data.deleteAttributeAt(index);
			}
		}
		
		//FileHandler.saveDatasetFile(method.name() + "-" + this.organism.originalDataset, this.organism, data);
		
		if(data.classIndex() == -1)
			data.setClassIndex(data.numAttributes() - 1);
		
		return data;
	}
}
