package br.uefs.analyst;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import br.uefs.model.GOTerm;
import br.uefs.util.FileHandler;
import br.uefs.util.ModelOrganism;
import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.AttributeSelection;
import weka.attributeSelection.Ranker;
import weka.core.Instances;

public class Ranking {
	
	public List<GOTerm> rank(ModelOrganism organism, String evaluatorPackage, Instances data) throws Exception{
		
		List<GOTerm> terms = new ArrayList<>();
		
		AttributeSelection filter = new AttributeSelection();

		Class<?> evaluatorClass = Class.forName(evaluatorPackage);

		Object evaluator = evaluatorClass.newInstance();

		Ranker rankerSearch = new Ranker();

		rankerSearch.setGenerateRanking(true);
		filter.setEvaluator((ASEvaluation) evaluator);
		filter.setSearch(rankerSearch);
		filter.SelectAttributes(data);
		
		int pos = 0;
		double previousRank = Double.MAX_VALUE;
		int[] indexes = Arrays.copyOfRange(filter.selectedAttributes(), 0, filter.selectedAttributes().length-1); //excludes class attribute
		double[][] ranks = filter.rankedAttributes();

		for(int index = 0; index < ranks.length; index++) {
			
			if(ranks[index][1] < previousRank)
				pos+=1;
			
			GOTerm term = new GOTerm(data.attribute(indexes[index]).name(), organism.name());
			
			term.setTermPos(pos);
			term.setRankValue(ranks[index][1]);

			terms.add(term);
			
			previousRank = ranks[index][1];
		}

		return terms;
	}
	
	public List<GOTerm> filterDataset(ModelOrganism organism, String evaluatorPackage) throws Exception {

		List<GOTerm> terms = new ArrayList<>();

		// load data set from path
		Instances data = FileHandler.readDatasetFile(organism.originalDataset, organism);
		AttributeSelection filter = new AttributeSelection();

		Class<?> evaluatorClass = Class.forName(evaluatorPackage);

		Object evaluator = evaluatorClass.newInstance();

		Ranker rankerSearch = new Ranker();

		rankerSearch.setGenerateRanking(true);
		filter.setEvaluator((ASEvaluation) evaluator);
		filter.setSearch(rankerSearch);
		filter.SelectAttributes(data);

		int pos = 1;
		int[] indexes = Arrays.copyOfRange(filter.selectedAttributes(), 0, filter.selectedAttributes().length-1); //excludes class attribute
		double[][] ranks = filter.rankedAttributes();

		for(int index = 0; index < ranks.length; index++) {
			
			GOTerm term = new GOTerm(data.attribute(indexes[index]).name(), organism.name());

			term.setTermPos(pos);
			term.setRankValue(ranks[index][1]);

			terms.add(term);
			
			pos++;
		}

		return terms;

	}
}
