package br.uefs.analyst;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import br.uefs.model.GOTerm;
import br.uefs.util.Graph;
import br.uefs.util.KendallCorrelation;
import br.uefs.util.ModelOrganism;
import br.uefs.util.RankingMethod;
import weka.core.Instances;

public class GOAnalyst {

	private Ranking filter;
	private String evaluationMethods[];

	private HashMap<ModelOrganism, List<List<GOTerm>>> partialSelectedTerms;
	private HashMap<ModelOrganism, List<GOTerm>> meanSelectedTerms;
	private HashMap<ModelOrganism, List<GOTerm>> medianSelectedTerms;
	private HashMap<ModelOrganism, List<GOTerm>> weightedMeanSelectedTerms;

	private HashMap<ModelOrganism, Graph> organismGraphs;

	private HashMap<ModelOrganism, List<List<GOTerm>>> filteredDataset;

	public GOAnalyst() {

		this.setOrganismGraphs(new HashMap<>());

		this.setEvaluationMethods(new String[] { "weka.attributeSelection.ChiSquaredAttributeEval",
				"weka.attributeSelection.GainRatioAttributeEval", "weka.attributeSelection.InfoGainAttributeEval",
				"weka.attributeSelection.ReliefFAttributeEval", "weka.attributeSelection.OneRAttributeEval",
				"weka.attributeSelection.SymmetricalUncertAttributeEval" });

		this.setFilter(new Ranking());
		this.setFilteredDataset(new HashMap<>());
		this.setPartialSelectedTerms(new HashMap<>());
		this.setMeanSelectedTerms(new HashMap<>());
		this.setMedianSelectedTerms(new HashMap<>());
		this.setWeightedMeanSelectedTerms(new HashMap<>());
	}

	public void run() {

		this.partialSelection();
		this.meanSelection();
		this.medianSelection();
		this.weightedMeanSelection();
	}

	public HashMap<RankingMethod, List<GOTerm>> rank(ModelOrganism organism, Instances data) throws Exception {

		HashMap<RankingMethod, List<GOTerm>> rankedTermsByMethod = new HashMap<>();

		for (RankingMethod method : RankingMethod.values()) {

			this.filter.rank(organism, method.evaluationPackage, data);
		}

		return rankedTermsByMethod;
	}

	public void setValuesOnGraph(ModelOrganism organism, HashMap<RankingMethod, List<GOTerm>> rankedTermsByMethod) {

		Graph graphClone = (Graph) this.organismGraphs.get(organism).clone();

		rankedTermsByMethod.forEach((method, rankedTerms) -> {

			this.setValuesOnTerms(graphClone, rankedTerms);
		});

	}

	public List<GOTerm> partialSelection(ModelOrganism organism, List<GOTerm> rankedTerms) {

		List<GOTerm> selectedTerms = new ArrayList<>();
		Graph graphClone = (Graph) this.organismGraphs.get(organism).clone();

		this.setValuesOnTerms(graphClone, rankedTerms);
		this.setValuesOnDescendantTerms(graphClone, rankedTerms);

		this.selectTerms(graphClone.getTerms().values(), selectedTerms);

		return selectedTerms;
	}

	public void partialSelection() {

		System.out.println("Partial selection");
		organismGraphs.forEach((organism, graph) -> {

			Graph graphClone = (Graph) graph.clone();

			List<List<GOTerm>> filteredTerms = filteredDataset.get(organism);
			List<List<GOTerm>> selectedTermsOverall = new ArrayList<>();

			for (List<GOTerm> terms : filteredTerms) {

				this.setValuesOnTerms(graphClone, terms);
				this.setValuesOnDescendantTerms(graphClone, terms);

				List<GOTerm> selectedTerms = new ArrayList<>();
				this.selectTerms(graphClone.getTerms().values(), selectedTerms);

				selectedTermsOverall.add(selectedTerms);
			}
			this.getPartialSelectedTerms().put(organism, selectedTermsOverall);

		});

	}

	public List<GOTerm> meanSelection(ModelOrganism organism,
			HashMap<RankingMethod, List<GOTerm>> rankedTermsByMethod) {

		List<GOTerm> selectedTerms = new ArrayList<>();
		Graph graphClone = (Graph) this.organismGraphs.get(organism).clone();

		this.defineMeanRanking(graphClone, rankedTermsByMethod.values());
		this.selectAvgTerms(graphClone.getTerms().values(), selectedTerms);

		this.getMeanSelectedTerms().put(organism, selectedTerms);

		return selectedTerms;
	}

	public List<GOTerm> weightedSelection(ModelOrganism organism,
			HashMap<RankingMethod, List<GOTerm>> rankedTermsByMethod) {

		List<GOTerm> selectedTerms = new ArrayList<>();
		List<List<Double>> correlationWeights = new ArrayList<>();
		Graph graphClone = (Graph) this.organismGraphs.get(organism).clone();

		KendallCorrelation.run(rankedTermsByMethod.values(), correlationWeights);
		this.removeDiagonalValues(correlationWeights);

		this.defineWeightedMeanRanking(graphClone, correlationWeights, rankedTermsByMethod.values());

		this.selectAvgTerms(graphClone.getTerms().values(), selectedTerms);

		this.getWeightedMeanSelectedTerms().put(organism, selectedTerms);

		return selectedTerms;
	}

	public List<GOTerm> medianSelection(ModelOrganism organism,
			HashMap<RankingMethod, List<GOTerm>> rankedTermsByMethod) {

		List<GOTerm> selectedTerms = new ArrayList<>();
		Graph graphClone = (Graph) this.organismGraphs.get(organism).clone();

		this.defineMedianRanking(graphClone, rankedTermsByMethod.values());
		this.selectAvgTerms(graphClone.getTerms().values(), selectedTerms);

		this.getMedianSelectedTerms().put(organism, selectedTerms);

		return selectedTerms;
	}

	public void meanSelection() {

		System.out.println("Mean selection");
		organismGraphs.forEach((organism, graph) -> {

			Graph graphClone = (Graph) graph.clone();

			List<List<GOTerm>> filteredTerms = filteredDataset.get(organism);
			List<GOTerm> selectedTerms = new ArrayList<>();

			this.defineMeanRanking(graphClone, filteredTerms);
			this.selectAvgTerms(graphClone.getTerms().values(), selectedTerms);

			this.getMeanSelectedTerms().put(organism, selectedTerms);
		});
	}

	public void medianSelection() {

		System.out.println("Median selection");
		organismGraphs.forEach((organism, graph) -> {

			Graph graphClone = (Graph) graph.clone();

			List<List<GOTerm>> filteredTerms = filteredDataset.get(organism);
			List<GOTerm> selectedTerms = new ArrayList<>();

			this.defineMedianRanking(graphClone, filteredTerms);
			this.selectAvgTerms(graphClone.getTerms().values(), selectedTerms);

			this.getMedianSelectedTerms().put(organism, selectedTerms);
		});
	}

	public void weightedMeanSelection() {

		System.out.println("Weighted Mean selection");
		organismGraphs.forEach((organism, graph) -> {

			Graph graphClone = (Graph) graph.clone();
			this.weightedMeanSelection(organism, graphClone);
		});
	}

	public List<GOTerm> weightedMeanSelection(ModelOrganism organism, Graph graphClone) {

		List<List<GOTerm>> filteredTerms = filteredDataset.get(organism);
		List<List<Double>> correlationWeights = new ArrayList<>();
		List<GOTerm> selectedTerms = new ArrayList<>();

		KendallCorrelation.run(filteredTerms, correlationWeights);
		this.removeDiagonalValues(correlationWeights);

		this.defineWeightedMeanRanking(graphClone, correlationWeights, filteredTerms);

		this.selectAvgTerms(graphClone.getTerms().values(), selectedTerms);

		this.getWeightedMeanSelectedTerms().put(organism, selectedTerms);

		return selectedTerms;
	}

	private void defineMeanRanking(Graph graphClone, Collection<List<GOTerm>> filteredTerms) {

		for (List<GOTerm> terms : filteredTerms) {

			// calculates a term total position
			for (GOTerm term : terms) {

				GOTerm termInGraph = graphClone.getTerms().get(term.getTerm());

				int currentPos = term.getTermPos();
				double currentSumPos = termInGraph.getAveragePos();

				termInGraph.setAveragePos(currentPos + currentSumPos);
			}
		}

		// calculates a term average position
		for (GOTerm term : graphClone.getTerms().values()) {

			double posValue = term.getAveragePos();
			double value = filteredTerms.size();

			term.setAveragePos(posValue / value);
		}

		reassignDescendantValues(graphClone.getTerms());

	}

	private void defineWeightedMeanRanking(Graph graphClone, List<List<Double>> correlationWeights,
			Collection<List<GOTerm>> filteredTerms) {

		List<Double> chosenWeights = this.chooseCorrelationWeights(correlationWeights);

		double sumWeights = this.sumWeights(chosenWeights);
		int index = 0;

		for (List<GOTerm> terms : filteredTerms) {

			double weight = chosenWeights.get(index);

			for (GOTerm term : terms) {

				int termPos = term.getTermPos();
				double weightedPos = termPos * weight;
				term.setAveragePos(weightedPos);
			}

			index += 1;
		}

		// sum all ranking values
		for (List<GOTerm> terms : filteredTerms) {

			for (GOTerm term : terms) {

				GOTerm termInGraph = graphClone.getTerms().get(term.getTerm());

				double currentPos = term.getAveragePos();
				double currentSumPos = termInGraph.getAveragePos();

				double sumPos = currentPos + currentSumPos;
				termInGraph.setAveragePos(sumPos);
			}
		}

		// divide by the sum of the weights
		for (GOTerm term : graphClone.getTerms().values()) {

			double sumPos = term.getAveragePos();
			double average = (sumPos / sumWeights);

			term.setAveragePos(average);
		}

		reassignDescendantValues(graphClone.getTerms());
	}

	private void defineMedianRanking(Graph graphClone, Collection<List<GOTerm>> filteredTerms) {

		for (List<GOTerm> terms : filteredTerms) {

			for (GOTerm term : terms) {

				GOTerm termInGraph = graphClone.getTerms().get(term.getTerm());

				int currentPos = term.getTermPos();
				termInGraph.getPositions().add(currentPos);
			}
		}

		for (GOTerm term : graphClone.getTerms().values()) {

			List<Integer> positions = term.getPositions();

			int size = positions.size();
			double median = 0;

			Collections.sort(positions);

			// if is even
			if (size % 2 == 0) {

				median = positions.get(size / 2) + positions.get((size / 2) - 1);
				median = median / 2;
			} else { // if is odd

				median = positions.get(size / 2);
			}

			term.setAveragePos(median);
		}

		reassignDescendantValues(graphClone.getTerms());

	}

	private void reassignDescendantValues(HashMap<String, GOTerm> terms) {

		for (GOTerm term : terms.values()) {

			for (GOTerm descendant : term.getDescendants()) {

				double avgPos = terms.get(descendant.getTerm()).getAveragePos();
				descendant.setAveragePos(avgPos);
			}
		}

	}

	private void setValuesOnTerms(Graph graphClone, List<GOTerm> terms) {

		for (GOTerm term : terms) {

			graphClone.getTerms().get(term.getTerm()).setRankValue(term.getRankValue());
			graphClone.getTerms().get(term.getTerm()).setTermPos(term.getTermPos());

		}
	}

	private void setValuesOnDescendantTerms(Graph graphClone, List<GOTerm> terms) {

		for (GOTerm term : terms) {

			for (GOTerm descendant : term.getDescendants()) {

				GOTerm descendantOnGraph = graphClone.getTerms().get(descendant);

				if (descendantOnGraph != null) {

					descendant.setRankValue(descendantOnGraph.getRankValue());
					descendant.setTermPos(descendantOnGraph.getTermPos());
				}
			}

		}
	}

	private void selectTerms(Collection<GOTerm> terms, List<GOTerm> selectedTerms) {

		for (GOTerm term : terms) {

			if (term.isBetterQualified()) {

				selectedTerms.add(term);
			}
		}
	}

	private void selectAvgTerms(Collection<GOTerm> terms, List<GOTerm> selectedTerms) {

		for (GOTerm term : terms) {

			if (term.isBetterQualifiedAvg()) {
				selectedTerms.add(term);
			}
		}
	}

	/**
	 * http://www.wikihow.com/Calculate-Spearman's-Rank-Correlation-Coefficient
	 * 
	 * This method removes the diagonal values in the correlation weights
	 * matrix, e.g., values belonging to the same row 0 and column 0, row 1 and
	 * column 1, on and on.
	 * 
	 * @param correlationWeights
	 */
	private void removeDiagonalValues(List<List<Double>> correlationWeights) {

		for (List<Double> weights : correlationWeights) {

			int index = correlationWeights.indexOf(weights);

			for (Double weight : weights) {

				int indexWeight = weights.indexOf(weight);

				if (index != indexWeight) {
					correlationWeights.get(indexWeight).remove(index);
					correlationWeights.get(indexWeight).add(index, weight);
				}
			}
		}
	}

	private List<Double> chooseCorrelationWeights(List<List<Double>> correlationWeights) {

		List<Double> chosenWeights = new ArrayList<>();

		for (List<Double> weights : correlationWeights) {

			int index = correlationWeights.indexOf(weights);
			double average = this.sumWeights(weights, index);

			average = (average) / (weights.size() - 1);

			double weight = 1 - Math.abs(average); // value between 0 and 1

			chosenWeights.add(weight);
		}

		return chosenWeights;
	}

	private double sumWeights(List<Double> chosenWeights) {

		double sum = 0;

		for (Double weight : chosenWeights) {

			sum += weight.doubleValue();
		}
		return sum;
	}

	private double sumWeights(List<Double> weights, int index) {

		double sum = 0;
		for (Double weight : weights) {
			int indexWeight = weights.indexOf(weight);

			if (index != indexWeight)
				sum += weight.doubleValue();
		}

		return sum;
	}

	public Ranking getFilter() {

		return filter;
	}

	public void setFilter(Ranking filter) {

		this.filter = filter;
	}

	public String[] getEvaluationMethods() {
		return evaluationMethods;
	}

	public void setEvaluationMethods(String evaluationMethods[]) {
		this.evaluationMethods = evaluationMethods;
	}

	public HashMap<ModelOrganism, List<List<GOTerm>>> getFilteredDataset() {
		return filteredDataset;
	}

	public void setFilteredDataset(HashMap<ModelOrganism, List<List<GOTerm>>> filteredDataset) {

		for (ModelOrganism organism : ModelOrganism.values()) {

			filteredDataset.put(organism, new ArrayList<>());
			for (String pack : evaluationMethods) {

				List<GOTerm> terms;
				try {

					terms = filter.filterDataset(organism, pack);
					filteredDataset.get(organism).add(terms);

				} catch (Exception e) {

					e.printStackTrace();
				}
			}
		}

		this.filteredDataset = filteredDataset;
	}

	public HashMap<ModelOrganism, Graph> getOrganismGraphs() {
		return organismGraphs;
	}

	public void setOrganismGraphs(HashMap<ModelOrganism, Graph> organismGraphs) {

		for (ModelOrganism organism : ModelOrganism.values()) {

			organismGraphs.put(organism, new Graph(organism));
		}

		this.organismGraphs = organismGraphs;
	}

	public HashMap<ModelOrganism, List<List<GOTerm>>> getPartialSelectedTerms() {
		return partialSelectedTerms;
	}

	public void setPartialSelectedTerms(HashMap<ModelOrganism, List<List<GOTerm>>> partialSelectedTerms) {
		this.partialSelectedTerms = partialSelectedTerms;
	}

	public HashMap<ModelOrganism, List<GOTerm>> getMeanSelectedTerms() {
		return meanSelectedTerms;
	}

	public void setMeanSelectedTerms(HashMap<ModelOrganism, List<GOTerm>> meanSelectedTerms) {
		this.meanSelectedTerms = meanSelectedTerms;
	}

	public HashMap<ModelOrganism, List<GOTerm>> getMedianSelectedTerms() {
		return medianSelectedTerms;
	}

	public void setMedianSelectedTerms(HashMap<ModelOrganism, List<GOTerm>> medianSelectedTerms) {
		this.medianSelectedTerms = medianSelectedTerms;
	}

	public HashMap<ModelOrganism, List<GOTerm>> getWeightedMeanSelectedTerms() {
		return weightedMeanSelectedTerms;
	}

	public void setWeightedMeanSelectedTerms(HashMap<ModelOrganism, List<GOTerm>> weightedMeanSelectedTerms) {
		this.weightedMeanSelectedTerms = weightedMeanSelectedTerms;
	}

}
