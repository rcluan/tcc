package br.uefs.run;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import br.uefs.analyst.GOAnalyst;
import br.uefs.model.Classifier;
import br.uefs.model.GOTerm;
import br.uefs.util.Calc;
import br.uefs.util.ModelOrganism;
import br.uefs.util.RankingMethod;
import weka.core.Instances;

public class RankedAnalysis {

	public static void main(String[] args) {
		
		try {
			
			int seed = 1;
			int folds = 10;
			
			GOAnalyst analyst = new GOAnalyst();
			
			for(ModelOrganism organism : ModelOrganism.values()){
				
				System.out.println(organism.name());
				
				Classifier classifier = new Classifier(organism);
				Instances originalDataset = classifier.getPreprocessor().getOriginalDataset();
				
				classifier.classifyKNNOriginalDataset();
				
				System.out.println("Std: " + classifier.getStdGMean());
				
				HashMap<RankingMethod, double[]> sensitivitiesKNN = new HashMap<>();
				HashMap<RankingMethod, double[]> specificitiesKNN = new HashMap<>();
				
				HashMap<RankingMethod, double[]> sensitivitiesNB = new HashMap<>();
				HashMap<RankingMethod, double[]> specificitiesNB = new HashMap<>();
				
				double[] aarSensitivitiesKNN = new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},
						 aarSpecificitiesKNN = new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
				
				double[] aarSensitivitiesNB = new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},
						 aarSpecificitiesNB= new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
				
				double[] warSensitivitiesKNN = new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},
						 warSpecificitiesKNN = new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
				
				double[] warSensitivitiesNB = new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},
						 warSpecificitiesNB= new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
				
				double[] mrSensitivitiesKNN = new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},
						 mrSpecificitiesKNN = new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
				
				double[] mrSensitivitiesNB = new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0},
						 mrSpecificitiesNB= new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
				
				for(RankingMethod method: RankingMethod.values()) {
					
					sensitivitiesKNN.put(method, new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0});
					specificitiesKNN.put(method, new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0});
					
					sensitivitiesNB.put(method, new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0});
					specificitiesNB.put(method, new double[] {0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0});
				}
				
				Random random = new Random(seed);

				originalDataset.randomize(random);
				originalDataset.stratify(folds);
				
				for (int n = 0; n < folds; n++) {
					
					//int fold_n = n+1;
					
					HashMap<RankingMethod, List<GOTerm>> rankedTermsByMethod = new HashMap<>();

					Instances training = originalDataset.trainCV(folds, n, random);
					Instances testing = originalDataset.testCV(folds, n);
					
					// partial selection
					for(RankingMethod method: RankingMethod.values()) {
						
						//System.out.println("Partial selection " + method.name() + " fold " + fold_n);
						
						List<GOTerm> rankedTerms = analyst.getFilter().rank(organism, method.evaluationPackage, training);
						rankedTermsByMethod.put(method, rankedTerms);
						
						
						List<GOTerm> selectedTerms = analyst.partialSelection(organism, rankedTerms);
						
						Instances selectedTermsTrainingDataset = classifier.getPreprocessor().buildFromDataset(training, selectedTerms);
						Instances selectedTermsTestingDataset = classifier.getPreprocessor().buildFromDataset(testing, selectedTerms); // maintains only selected terms on testing dataset
						
						double[] valuesKNN = classifier.classifyKNN(selectedTermsTrainingDataset, selectedTermsTestingDataset);
						double[] valuesNB = classifier.classifyNaiveBayes(selectedTermsTrainingDataset, selectedTermsTestingDataset);
						
						double[] currentSensitivityKNN = sensitivitiesKNN.get(method);
						double[] currentSpecificityKNN = specificitiesKNN.get(method);
						
						double[] currentSensitivityNB = sensitivitiesNB.get(method);
						double[] currentSpecificityNB = specificitiesNB.get(method);
						
						currentSensitivityKNN[n] = valuesKNN[0];
						currentSpecificityKNN[n] = valuesKNN[1];
						
						currentSensitivityNB[n] = valuesNB[0];
						currentSpecificityNB[n] = valuesNB[1];
						
						sensitivitiesKNN.put(method, currentSensitivityKNN);
						specificitiesKNN.put(method, currentSpecificityKNN);
						
						sensitivitiesNB.put(method, currentSensitivityNB);
						specificitiesNB.put(method, currentSpecificityNB);
						
					}
					
					List<GOTerm> meanSelectedTerms = analyst.meanSelection(organism, rankedTermsByMethod);
					List<GOTerm> weightedSelectedTerms = analyst.weightedSelection(organism, rankedTermsByMethod);
					List<GOTerm> medianSelectedTerms = analyst.medianSelection(organism, rankedTermsByMethod);
					
					Instances meanSelectedTermsTrainingDataset = classifier.getPreprocessor().buildFromDataset(training, meanSelectedTerms);
					Instances meanSelectedTermsTestingDataset = classifier.getPreprocessor().buildFromDataset(testing, meanSelectedTerms); 
					
					Instances medianSelectedTermsTrainingDataset = classifier.getPreprocessor().buildFromDataset(training, medianSelectedTerms);
					Instances medianSelectedTermsTestingDataset = classifier.getPreprocessor().buildFromDataset(testing, medianSelectedTerms); 
					
					Instances weightedSelectedTermsTrainingDataset = classifier.getPreprocessor().buildFromDataset(training, weightedSelectedTerms);
					Instances weightedSelectedTermsTestingDataset = classifier.getPreprocessor().buildFromDataset(testing, weightedSelectedTerms); 
					
					double[] arr_values_knn = classifier.classifyKNN(meanSelectedTermsTrainingDataset, meanSelectedTermsTestingDataset);
					double[] mr_values_knn = classifier.classifyKNN(medianSelectedTermsTrainingDataset, medianSelectedTermsTestingDataset);
					double[] war_values_knn = classifier.classifyKNN(weightedSelectedTermsTrainingDataset, weightedSelectedTermsTestingDataset);
					
					double[] arr_values_nb = classifier.classifyNaiveBayes(meanSelectedTermsTrainingDataset, meanSelectedTermsTestingDataset);
					double[] mr_values_nb = classifier.classifyNaiveBayes(medianSelectedTermsTrainingDataset, medianSelectedTermsTestingDataset);
					double[] war_values_nb = classifier.classifyNaiveBayes(weightedSelectedTermsTrainingDataset, weightedSelectedTermsTestingDataset);
					
					aarSensitivitiesKNN[n] = arr_values_knn[0];
					aarSpecificitiesKNN[n] = arr_values_knn[1];
					
					mrSensitivitiesKNN[n] = mr_values_knn[0];
					mrSpecificitiesKNN[n] = mr_values_knn[1];
					
					warSensitivitiesKNN[n] = war_values_knn[0];
					warSpecificitiesKNN[n] = war_values_knn[1];
					
					aarSensitivitiesNB[n] = arr_values_nb[0];
					aarSpecificitiesNB[n] = arr_values_nb[1];
					
					mrSensitivitiesNB[n] = mr_values_nb[0];
					mrSpecificitiesNB[n] = mr_values_nb[1];
					
					warSensitivitiesNB[n] = war_values_nb[0];
					warSpecificitiesNB[n] = war_values_nb[1];
					
				}

				
				for(RankingMethod method: RankingMethod.values()) {
					
					double[] methodSensitivities = sensitivitiesKNN.get(method), methodSpecificities = specificitiesKNN.get(method);
					
					double totalSensitivities = Calc.total(methodSensitivities),
						   totalSpecificities = Calc.total(methodSpecificities),
						   sensitivitiesMean = Calc.mean(methodSensitivities),
						   specificityMean = Calc.mean(methodSpecificities),
						   sensitivitiesStdDeviation = Calc.stdDeviation(methodSensitivities, sensitivitiesMean),
						   specificityStdDeviation = Calc.stdDeviation(methodSpecificities, specificityMean),
						   sensitivitiesStdError = Calc.stdError(sensitivitiesStdDeviation, folds),
						   specificityStdError = Calc.stdError(specificityStdDeviation, folds),
						   gmean = Calc.gmean(totalSensitivities, totalSpecificities);
					
					System.out.println(method.name() + ": " + gmean*100.0 + " (std. error sens/spec) " + sensitivitiesStdError*100.0 + "/" + specificityStdError*100.0);
				}
				
				double aarTotalSensitivitiesKNN = Calc.total(aarSensitivitiesKNN),
					   aarTotalSpecificitiesKNN = Calc.total(aarSpecificitiesKNN),
					   aarSensitivitiesKNNMean = Calc.mean(aarSensitivitiesKNN),
					   aarSpecificityMean = Calc.mean(aarSpecificitiesKNN),
					   aarSensitivitiesKNNStdDeviation = Calc.stdDeviation(aarSensitivitiesKNN, aarSensitivitiesKNNMean),
					   aarSpecificityStdDeviation = Calc.stdDeviation(aarSpecificitiesKNN, aarSpecificityMean),
					   aarSensitivitiesKNNStdError = Calc.stdError(aarSensitivitiesKNNStdDeviation, folds),
					   aarSpecificityStdError = Calc.stdError(aarSpecificityStdDeviation, folds),
					   aarGmean = Calc.gmean(aarTotalSensitivitiesKNN, aarTotalSpecificitiesKNN);
				
				System.out.println("AAR: " + aarGmean*100.0 + " (std. error sens/spec) " + aarSensitivitiesKNNStdError*100.0 + "/" + aarSpecificityStdError*100.0);
				
				double warTotalSensitivitiesKNN = Calc.total(warSensitivitiesKNN),
					   warTotalSpecificitiesKNN = Calc.total(warSpecificitiesKNN),
					   warSensitivitiesKNNMean = Calc.mean(warSensitivitiesKNN),
					   warSpecificityMean = Calc.mean(warSpecificitiesKNN),
					   warSensitivitiesKNNStdDeviation = Calc.stdDeviation(warSensitivitiesKNN, warSensitivitiesKNNMean),
					   warSpecificityStdDeviation = Calc.stdDeviation(warSpecificitiesKNN, warSpecificityMean),
					   warSensitivitiesKNNStdError = Calc.stdError(warSensitivitiesKNNStdDeviation, folds),
					   warSpecificityStdError = Calc.stdError(warSpecificityStdDeviation, folds),
					   warGmean = Calc.gmean(warTotalSensitivitiesKNN, warTotalSpecificitiesKNN);
					
				System.out.println("WAR: " + warGmean*100.0 + " (std. error sens/spec) " + warSensitivitiesKNNStdError*100.0 + "/" + warSpecificityStdError*100.0);
					
				double mrTotalSensitivitiesKNN = Calc.total(mrSensitivitiesKNN),
					   mrTotalSpecificitiesKNN = Calc.total(mrSpecificitiesKNN),
					   mrSensitivitiesKNNMean = Calc.mean(mrSensitivitiesKNN),
					   mrSpecificityMean = Calc.mean(mrSpecificitiesKNN),
					   mrSensitivitiesKNNStdDeviation = Calc.stdDeviation(mrSensitivitiesKNN, mrSensitivitiesKNNMean),
					   mrSpecificityStdDeviation = Calc.stdDeviation(mrSpecificitiesKNN, mrSpecificityMean),
					   mrSensitivitiesKNNStdError = Calc.stdError(mrSensitivitiesKNNStdDeviation, folds),
					   mrSpecificityStdError = Calc.stdError(mrSpecificityStdDeviation, folds),
					   mrGmean = Calc.gmean(mrTotalSensitivitiesKNN, mrTotalSpecificitiesKNN);
					
				System.out.println("MR: " + mrGmean*100.0 + " (std. error sens/spec) " + mrSensitivitiesKNNStdError*100.0 + "/" + mrSpecificityStdError*100.0);
				
				System.out.println("-------------Naive Bayes-------------");
				
				classifier.classifyNaiveBayesOriginalDataset();
				
				System.out.println("Std: " + classifier.getStdGMean());
				
				for(RankingMethod method: RankingMethod.values()) {
					
					double[] methodSensitivities = sensitivitiesNB.get(method), methodSpecificities = specificitiesNB.get(method);
					
					double totalSensitivities = Calc.total(methodSensitivities),
						   totalSpecificities = Calc.total(methodSpecificities),
						   sensitivitiesMean = Calc.mean(methodSensitivities),
						   specificityMean = Calc.mean(methodSpecificities),
						   sensitivitiesStdDeviation = Calc.stdDeviation(methodSensitivities, sensitivitiesMean),
						   specificityStdDeviation = Calc.stdDeviation(methodSpecificities, specificityMean),
						   sensitivitiesStdError = Calc.stdError(sensitivitiesStdDeviation, folds),
						   specificityStdError = Calc.stdError(specificityStdDeviation, folds),
						   gmean = Calc.gmean(totalSensitivities, totalSpecificities);
					
					System.out.println(method.name() + ": " + gmean*100.0 + " (std. error sens/spec) " + sensitivitiesStdError*100.0 + "/" + specificityStdError*100.0);
				}
				
				double aarTotalSensitivitiesNB = Calc.total(aarSensitivitiesNB),
						   aarTotalSpecificitiesNB = Calc.total(aarSpecificitiesNB),
						   aarSensitivitiesNBMean = Calc.mean(aarSensitivitiesNB),
						   aarSpecificityNBMean = Calc.mean(aarSpecificitiesNB),
						   aarSensitivitiesNBStdDeviation = Calc.stdDeviation(aarSensitivitiesNB, aarSensitivitiesNBMean),
						   aarSpecificityNBStdDeviation = Calc.stdDeviation(aarSpecificitiesNB, aarSpecificityNBMean),
						   aarSensitivitiesNBStdError = Calc.stdError(aarSensitivitiesNBStdDeviation, folds),
						   aarSpecificityNBStdError = Calc.stdError(aarSpecificityNBStdDeviation, folds),
						   aarGmeanNB = Calc.gmean(aarTotalSensitivitiesNB, aarTotalSpecificitiesNB);
					
					System.out.println("AAR: " + aarGmeanNB*100.0 + " (std. error sens/spec) " + aarSensitivitiesNBStdError*100.0 + "/" + aarSpecificityNBStdError*100.0);
					
					double warTotalSensitivitiesNB = Calc.total(warSensitivitiesNB),
						   warTotalSpecificitiesNB = Calc.total(warSpecificitiesNB),
						   warSensitivitiesNBMean = Calc.mean(warSensitivitiesNB),
						   warSpecificityNBMean = Calc.mean(warSpecificitiesNB),
						   warSensitivitiesNBStdDeviation = Calc.stdDeviation(warSensitivitiesNB, warSensitivitiesNBMean),
						   warSpecificityNBStdDeviation = Calc.stdDeviation(warSpecificitiesKNN, warSpecificityNBMean),
						   warSensitivitiesNBStdError = Calc.stdError(warSensitivitiesNBStdDeviation, folds),
						   warSpecificityNBStdError = Calc.stdError(warSpecificityNBStdDeviation, folds),
						   warGmeanNB = Calc.gmean(warTotalSensitivitiesNB, warTotalSpecificitiesNB);
						
					System.out.println("WAR: " + warGmeanNB*100.0 + " (std. error sens/spec) " + warSensitivitiesNBStdError*100.0 + "/" + warSpecificityNBStdError*100.0);
						
					double mrTotalSensitivitiesNB = Calc.total(mrSensitivitiesNB),
						   mrTotalSpecificitiesNB = Calc.total(mrSpecificitiesNB),
						   mrSensitivitiesNBMean = Calc.mean(mrSensitivitiesNB),
						   mrSpecificityNBMean = Calc.mean(mrSpecificitiesNB),
						   mrSensitivitiesNBStdDeviation = Calc.stdDeviation(mrSensitivitiesNB, mrSensitivitiesNBMean),
						   mrSpecificityNBStdDeviation = Calc.stdDeviation(mrSpecificitiesNB, mrSpecificityNBMean),
						   mrSensitivitiesNBStdError = Calc.stdError(mrSensitivitiesNBStdDeviation, folds),
						   mrSpecificityNBStdError = Calc.stdError(mrSpecificityNBStdDeviation, folds),
						   mrGmeanNB = Calc.gmean(mrTotalSensitivitiesNB, mrTotalSpecificitiesNB);
						
					System.out.println("MR: " + mrGmeanNB*100.0 + " (std. error sens/spec) " + mrSensitivitiesNBStdError*100.0 + "/" + mrSpecificityNBStdError*100.0);
					
				
				
			}
			
			/*
			analyst.run();
			for(ModelOrganism organism : ModelOrganism.values()){
				
				System.out.println(organism.name());
				Classifier classifier = new Classifier(
						organism,
						analyst.getPartialSelectedTerms(),
						analyst.getMeanSelectedTerms(),
						analyst.getMedianSelectedTerms(),
						analyst.getWeightedMeanSelectedTerms());
				
				classifier.classifyNaiveBayesOriginalDataset();
				classifier.classifyNaiveBayes();
				
				System.out.println(classifier.getStdGMean());
				
				classifier.getgMeans().forEach((method, gmean) -> {
					
					System.out.println(gmean);
				});
				System.out.println("//////////////////////////////////////////////////////////////");
			}
			
			//classifier.classifyKNNOriginalDatasetAvg();
			//classifier.classifyKNNAvg();
			
			*/
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
