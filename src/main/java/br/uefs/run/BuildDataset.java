package br.uefs.run;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.uefs.model.Instance;
import br.uefs.util.FileHandler;
import br.uefs.util.ModelOrganism;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;

public class BuildDataset {

	public static void main(String[] args) {

		try {

			List<String> possibleValues = new ArrayList<>();

			possibleValues.add("0");
			possibleValues.add("1");
			
			System.out.println("Model organism\t\t" + "Number of attributes\t\t" + "Number of instances");
			for (ModelOrganism organism : ModelOrganism.values()) {

				String[] originalDatasetSplit = organism.originalDataset.split("-");
				String relation = originalDatasetSplit[0] + "-" + originalDatasetSplit[1];

				ArrayList<Attribute> atts = new ArrayList<>();
				Instances data;
				double[] vals;

				HashMap<String, List<Integer>> terms_relation = FileHandler.mapTerms(organism);
				HashMap<String, Instance> genes_relation = FileHandler.mapGenes(organism);

				for (String term : terms_relation.keySet()) {

					atts.add(new Attribute(term, possibleValues));
				}

				atts.add(new Attribute("class", possibleValues));

				data = new Instances(relation, atts, 0);

				for (String gene : genes_relation.keySet()) {
					
					Instance instance = genes_relation.get(gene);
					int index = 0;
					vals = new double[data.numAttributes()];
					
					for (String term : terms_relation.keySet()) {
						
						if(instance.getGOTerms().contains(term))
							vals[index] = 1;
						
						index++;
					}
					
					vals[data.numAttributes()-1] = instance.getClassValue();
					
					data.add(new DenseInstance(1.0, vals));
				}
				
				
				FileHandler.saveDatasetFile(relation, organism, data);
				System.out.println(organism.name() + "\t\t\t" + terms_relation.size() + "\t\t\t\t" + genes_relation.size());
				System.out.println("====================================================================================");
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
